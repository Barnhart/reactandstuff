package ca.barnhart.reactandstuff.repositories;

import ca.barnhart.reactandstuff.model.Character;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CharacterRepository extends CrudRepository<Character, Long> {
    List<Character> findByName(String name);
    Character findById(int id);
}

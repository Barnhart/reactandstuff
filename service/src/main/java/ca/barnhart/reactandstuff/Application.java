package ca.barnhart.reactandstuff;

import ca.barnhart.reactandstuff.model.Character;
import ca.barnhart.reactandstuff.repositories.CharacterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.simp.SimpMessagingTemplate;

@SpringBootApplication
public class Application {
    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner demo(CharacterRepository repository) {
        return (args) -> {
            // save a few customers
            repository.save(Character.of("Jack Bauer"));
            repository.save(Character.of("Chloe O'Brian"));
            repository.save(Character.of("Kim Bauer"));
            repository.save(Character.of("David Palmer"));
            repository.save(Character.of("Michelle Dessler"));

            // fetch all customers
            log.info("Customers found with findAll():");
            log.info("-------------------------------");
            for (Character character : repository.findAll()) {
                log.info(character.toString());
            }
            log.info("");

            // fetch an individual customer by ID
            Character character = repository.findById(1);
            log.info("Character found with findById(1L):");
            log.info("--------------------------------");
            log.info(character.toString());
            log.info("");

            // fetch customers by last name
            log.info("Character found with findByName('Jack Bauer'):");
            log.info("--------------------------------------------");
            repository.findByName("Jack Bauer").forEach(bauer -> {
                log.info(bauer.toString());
            });
            log.info("");
        };
    }
}

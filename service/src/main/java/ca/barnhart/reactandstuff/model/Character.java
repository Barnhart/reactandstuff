package ca.barnhart.reactandstuff.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor(staticName="of")
public class Character {
    @Id
    @GeneratedValue
    private Integer id;

    @NonNull
    private String name;
}

package ca.barnhart.reactandstuff.model;

import lombok.Data;

@Data
public class WebSocketMessage {
    private String type;
    private String payload;
}

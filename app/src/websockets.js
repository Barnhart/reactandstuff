const createWebsocketWrapper = websocket => {
    const messageObservers = [];
    const errorObservers = [];

    websocket.onmessage = event => {
        console.log('websocket message received: ' + event);
        console.dir(event);
        const jsonData = JSON.parse(event.data);
        messageObservers.forEach(messageObserver => messageObserver(jsonData));
    };

    websocket.onerror = event => {
        console.log('websocket error: ' + event);
        console.dir(event);
        errorObservers.forEach(errorObserver => errorObserver(event.data));
    };

    return {
        addMessageObserver: messageObserver => messageObservers.push(messageObserver),
        addErrorObserver: errorObserver => errorObservers.push(errorObserver),
        removeMessageObserver: messageObserver => messageObservers.filter(item => item !== messageObserver),
        removeErrorObserver: errorObserver => errorObservers.filter(item => item !== errorObserver),
        send: data => websocket.send(data),
        close: () => websocket.close()
    };
};

export const create = uri => new Promise((resolve, reject) => {
    console.log('opening websocket...');

    let websocket;
    let socketWrapper;
    try {
        websocket = new WebSocket(uri);
        socketWrapper = createWebsocketWrapper(websocket);
    } catch (error) {
        console.log('websocket open error');
        console.dir(error);
        reject(error);
    }

    websocket.onopen = () => {
        console.log('websocket open');
        console.dir(socketWrapper);
        resolve(socketWrapper);
    };
});
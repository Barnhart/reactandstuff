import {all, call, put, take, takeEvery} from 'redux-saga/effects';
import {eventChannel} from 'redux-saga';
import {
    HANDLE_WEBSOCKET_ERROR,
    SEND_WEBSOCKET_MESSAGE,
    SUBSCRIBE_TO_WEBSOCKET_TOPIC,
    SUBSCRIBE_TO_WEBSOCKET_TOPIC_SUCCESS
} from '../actions/types';
import {handleWebsocketError, subscribeToWebsocketTopicSuccess} from '../actions/websocket';
import * as websockets from '../../websockets';

function* subscribeToWebsocketTopic(action) {
    try {
        const websocket = yield call(websockets.create, action.payload);
        yield put(subscribeToWebsocketTopicSuccess(websocket));
    } catch (error) {
        yield put(handleWebsocketError(error))
    }
}

// This function creates an event channel from a given socket
// Setup subscription to incoming `message` events
function createSocketChannel(websocket) {
    // `eventChannel` takes a subscriber function
    // the subscriber function takes an `emit` argument to put messages onto the channel
    return eventChannel(emit => {
        // puts event payload into the channel
        // this allows a Saga to take this payload from the returned channel
        // Take the data or error and create an action out of it
        websocket.addMessageObserver(data => {
            const type = data.type;
            const payload = data.payload;
            emit({
                type,
                payload
            });
        });

        websocket.addErrorObserver(error => {
            emit({
                type: HANDLE_WEBSOCKET_ERROR,
                error
            });
        });

        // the subscriber must return an unsubscribe function
        // this will be invoked when the saga calls `channel.close` method
        return () => websocket.close;
    })
}

function* watchOnMessages(action) {
    const websocket = action.payload;
    const websocketChannel = yield call(createSocketChannel, websocket);

    while (true) {
        try {
            const action = yield take(websocketChannel);
            yield put(action);
        } catch(error) {
            yield put(handleWebsocketError(error))
        }
    }
}

function* sendWebsocketMessage(action) {
    const websocket = action.payload.websocket;
    const message = action.payload.message;
    try {
        yield call(websocket.send, message)
    } catch (error) {
        yield put(handleWebsocketError(error))
    }
}

function* start() {
    yield put({type: SUBSCRIBE_TO_WEBSOCKET_TOPIC, payload: "ws://localhost:8080/app"});
}

export default function* rootSaga() {
    yield all([
        start(),
        takeEvery(SUBSCRIBE_TO_WEBSOCKET_TOPIC, subscribeToWebsocketTopic),
        takeEvery(SUBSCRIBE_TO_WEBSOCKET_TOPIC_SUCCESS, watchOnMessages),
        takeEvery(SEND_WEBSOCKET_MESSAGE, sendWebsocketMessage)
    ]);
}
import {combineReducers} from 'redux';
import characters from './characters';
import websocket from './websocket';

export default combineReducers({
    characters,
    websocket
});

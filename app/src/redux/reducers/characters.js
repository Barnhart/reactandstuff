import {ADD_CHARACTER} from '../actions/types';

const initialState = [];

const characters = (state = initialState, action) => {
    switch (action.type) {
        case ADD_CHARACTER:
            const id = action.payload.id;
            const name = action.payload.name;
            return {
                ...state,
                [id]: {
                    id,
                    name
                }
            };
        default:
            return state;
    }
};

export default characters;
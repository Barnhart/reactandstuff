import {
    HANDLE_WEBSOCKET_ERROR,
    RECEIVE_WEBSOCKET_MESSAGE,
    SUBSCRIBE_TO_WEBSOCKET_TOPIC_SUCCESS
} from '../actions/types';

const initialState = {
    messages: []
};

const websocket = (state = initialState, action) => {
    switch (action.type) {
        case RECEIVE_WEBSOCKET_MESSAGE:
            const id = action.payload.id;
            const message = action.payload.message;
            return {
                ...state,
                messages: {
                    ...state.messages,
                    [id]: {
                        id,
                        message
                    }
                }
            };
        case SUBSCRIBE_TO_WEBSOCKET_TOPIC_SUCCESS:
            return action.payload;
        case HANDLE_WEBSOCKET_ERROR:
            const error = action.error;
            return {
                ...state,
                error
            };
        default:
            return state;
    }
};

export default websocket;
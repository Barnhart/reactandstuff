import {
    SUBSCRIBE_TO_WEBSOCKET_TOPIC,
    SUBSCRIBE_TO_WEBSOCKET_TOPIC_SUCCESS,
    HANDLE_WEBSOCKET_ERROR,
    SEND_WEBSOCKET_MESSAGE,
    RECEIVE_WEBSOCKET_MESSAGE} from './types'

export const subscribeToWebsocketTopic = (uri, topic) => ({
    type: SUBSCRIBE_TO_WEBSOCKET_TOPIC,
    payload: {
        uri,
        topic
    }
});

export const subscribeToWebsocketTopicSuccess = websocket => ({
    type: SUBSCRIBE_TO_WEBSOCKET_TOPIC_SUCCESS,
    payload: websocket
});

export const handleWebsocketError = error => ({
    type: HANDLE_WEBSOCKET_ERROR,
    error
});

export const sendWebsocketMessage = (websocket, message) => ({
    type: SEND_WEBSOCKET_MESSAGE,
    payload: {
        websocket,
        message: JSON.stringify({
            type: RECEIVE_WEBSOCKET_MESSAGE,
            message
        })
    }
});

export const receiveWebsocketMessage = message => ({
    type: RECEIVE_WEBSOCKET_MESSAGE,
    payload: message
});
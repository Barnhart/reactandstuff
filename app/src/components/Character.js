import React from 'react';
import PropTypes from 'prop-types';

const Character = ({ name }) => (
    <li><p>{name}</p></li>
);

Character.propTypes = {
    name: PropTypes.string.isRequired
};

export default Character;

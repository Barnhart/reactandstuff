import React from 'react';
import PropTypes from 'prop-types';
import Character from './Character';

const CharacterList = ({ characters, websocket, sendMessage }) => (
    <div>
        <button onClick={() => sendMessage(websocket)}>
            Send Message
        </button>
        <ul>
            {characters.map(character =>
                <Character key={character.id} {...character} />
            )}
        </ul>
    </div>
);

CharacterList.propTypes = {
    characters: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired
        })
    )
};

export default CharacterList;

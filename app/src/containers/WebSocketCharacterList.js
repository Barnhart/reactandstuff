import { connect } from 'react-redux';
import { subscribeToWebsocketTopic, sendWebsocketMessage } from '../redux/actions/websocket';
import CharacterList from '../components/CharacterList';

const mapStateToProps = state => {
    console.log(state);

    return ({
        characters: state.characters,
        websocket: state.websocket
    });
};

const mapDispatchToProps = dispatch => ({
    sendMessage: websocket => dispatch(sendWebsocketMessage(websocket, "test " + Date.now()))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CharacterList);
